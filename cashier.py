# Program : CASHIER SIMULATION
# Author  : Muhammad Adzkia
# Email   : muhammadadzkia@live.com


import numpy as np
import pandas

item_data  = {  'Item':  ['Susu', 'Masker', 'Lampu', 'Daging', 'Apel'],
                'Harga': [50000, 25000, 15000, 20000, 30000],
                'Promo': ['Yes','Yes','No','No','No']}    
index_label = [1,2,3,4,5]
data = pandas.DataFrame(item_data, index_label)
total_belanja = 0

def mainMenu():
    print('=' * 20, 'CASHIER SIMULATION', '=' * 20)
    print('1. Promotional Items')
    print('2. All Item')
    print('3. Exit Program \n') 
    
    select = input('Silakan Pilih Program Kami: ')
    print('=' * 60)

    if select == '1':
        promotionalItem()
    elif select == '2':
        allItem()
    elif select == '3':
        print('Terima Kasih :)')
    else:
        print('Input salah \nProgram Close \n')


def promotionalItem():
    print('FlashSale UP to 99%')
    data_promo = data[data.Promo == 'Yes']
    print(data_promo[['Item', 'Harga']])
    print('=' * 60)
    return selectItem(data_promo)


def allItem():
    print('Silakan Pilih Item')
    print(data[['Item', 'Harga', 'Promo']])
    print('=' * 60)
    return selectItem(data)


def selectItem(data_input):
    global total_belanja
    data = data_input
    select_item = int(input('Silakan Pilih Item: '))
    if select_item > len(data.index):
        print('Input salah \nProgram Close \n')
        exit()
    qty_item = int(input ('Quantity: '))

    for i in range (10):
        if i == select_item:
            item = data.loc[i,'Item']
            harga = data.loc[i, 'Harga']
            total_harga = harga * qty_item
            total_belanja = total_belanja + total_harga
            print("Anda Membeli", item, "Sebanyak", qty_item, "pcs")
            print("Total Harga:   ", total_harga)
            print("Total Belanja: ", total_belanja, "\n")

    select_item = input("Lanjutkan Belanja? (Y/N): ")
    if select_item == 'N' or select_item == 'n':
        print('=' * 60)
        print("Total Belanja Anda: ", total_belanja, "\n")
        print("Terima Kasih - HAPPY SHOPING :)\n")
        print('=' * 60)
        return exit()

    elif select_item == 'Y' or select_item == 'y':
        return selectItem(data)

if __name__=="__main__":
    mainMenu()
